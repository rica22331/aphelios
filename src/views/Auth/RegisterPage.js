import { Toaster, toast } from "react-hot-toast";
import React, { useState } from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { NavLink, useHistory } from "react-router-dom";

import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Button from "@mui/material/Button";
import SendIcon from "@mui/icons-material/Send";

import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import axios from "axios";

// function Copyright(props) {
//   return (
//     <Typography
//       variant="body2"
//       color="text.secondary"
//       align="center"
//       {...props}
//     >
//       {"Copyright © "}
//       <Link color="inherit" href="https://mui.com/">
//         APHELIOS{" "}
//       </Link>{" "}
//       {new Date().getFullYear()}
//       {"."}
//     </Typography>
//   );
// }

const theme = createTheme();

export default function RegisterPage() {
  const urlBack = "https://13ef-2800-484-888d-6200-548a-d4e5-bdaa-651e.ngrok.io";
  let history = useHistory();

  const initialState = {
    name: "",
    lastName: "",
    mobile: "",
    email: "",
    documentType: "",
    numberDocument: "",
    role: "",
    state: "",
    password: "",
  };

  const [formComplete, setFormComplete] = useState(initialState);
  const [documentTypee, setDocumentTypee] = useState("");
  const [formChanged, setFormChanged] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.currentTarget;
    setFormComplete({ ...formComplete, [name]: value });
    if (
      formComplete.name !== "" &&
      formComplete.lastName !== "" &&
      formComplete.mobile !== "" &&
      formComplete.email !== "" &&
      documentTypee !== "" &&
      formComplete.numberDocument !== "" &&
      formComplete.password !== ""
    ) {
      setFormChanged(true);
    } else {
      setFormChanged(false);
    }
  };
  const handleChange = (event) => {
    setDocumentTypee(event.target.value);
  };
  const toastSuccess = () => {
    toast.success("Form submitted correctly", {
      position: "top-center",
    });
  };
  const onSubmit = (e) => {
    e.preventDefault();
    const postData = {
      name: formComplete.name,
      lastName: formComplete.lastName,
      mobile: formComplete.mobile,
      email: formComplete.email,
      documentType: documentTypee,
      numberDocument: formComplete.numberDocument,
      // role: "user",
      // state: "Activo",
      password: formComplete.password,
    };

    console.log(postData);

    axios
      .post(
        `${urlBack}/api/v1/user/signUp`,
        postData,

        {
          // headers: {
          //     'Content-Type': "application/json",
          //     'Authorization':"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1MzE1YWE5ZDYyMzVlYjc3YWZjYjYxMjNiMmMzZTZiNGUwZTI3MmQ4NjRkNDY0Nzk3MDEwNTZlMjk4ZDg4NWZmYWRjZWNkNjc5OWIwNDZhIn0.eyJhdWQiOiI1IiwianRpIjoiYTUzMTVhYTlkNjIzNWViNzdhZmNiNjEyM2IyYzNlNmI0ZTBlMjcyZDg2NGQ0NjQ3OTcwMTA1NmUyOThkODg1ZmZhZGNlY2Q2Nzk5YjA0NmEiLCJpYXQiOjE2MzgxMzE2OTMsIm5iZiI6MTYzODEzMTY5MywiZXhwIjoxNjY5NjY3NjkzLCJzdWIiOiIiLCJzY29wZXMiOltdfQ.5i47o1MHQtDGxUpu4LFZdjctsc-wQ0mcC_hYTdMLRZMOVfBdDf6_fpOcodG-ZhBhlfM9EPcAYaiWn3Qvbuii6B0ogosC8Sr7bkPq111EFb-Z2mkH0AXP9IPRGFfGkNrvYYLGRxxztZkUbAgGwPw9Qgbm1REwsvLBlOoxPYeczfINVuGauLRt1w0a_Zow6x51XF2LYbp0X2FXrCItAm3sZUnjC04tjLp1D4Zso-1OmGEKhM2iXfl6koexUb_Z42268XrL-6F7sy4QKucBl28YHalDEUYefV_OCfAxjCI55rGRAqKdrS5S3VG-39zvaDDbqa3Re7elelxcU6FNCOFvSS-S1-Mddxtiz_07Qp2jTcOW7wRM2MsD-z3heDvXiVOlP4s3GncLG71pmEBZ2vNs83WpYyS70cv7q6Al0tzKSKxvDFvjTwJAMMAl1DooNG5wq4OVQw6gKlYcrMEUp3cMFhfsY6pZnr3ZZKx2e7ZN0wUZEGcmn6LZ_iYzGdP5JhdmrzWJz_sQB2QkonJPgKmbpkpMlDJRrt8EqdwAK6SizUMmJUXVYUPwYL52AfpP-MvlD1hdtum4tr_LBA2vIUPjZiEGmkqSvSS9HaabXiHxNYASakqfb4A4uU5Uptl4OQuCgW5DaSDJ2zaAFx-q1LWKjGVM0F3dTMIQME4h6UUs4a0",
          // },
        }
      )
      .then((response) => {
        toastSuccess();
        setFormComplete(initialState);

        console.log("response", response.data.token);
        window.localStorage.setItem("userToken", response.data.token);
        history.push("/index");
      })
      .catch((error) => {
        toast.error(error.response.data.message, {
          position: "top-center",
        });
        console.log(error.response.data.message);
      });
  };

  return (
    <ThemeProvider theme={theme}>
      <Toaster />
      <Grid container component="main" sx={{ height: "90vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={3}
          md={7}
          sx={{
            backgroundImage: "url(https://i.pinimg.com/originals/c0/23/3e/c0233e6498e31b0dffee19dacc982cea.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
            <Box component="form" noValidate onSubmit={onSubmit} sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="name"
                label="Nombre"
                name="name"
                autoComplete="nombre"
                autoFocus
                onChange={handleInputChange}
                value={formComplete.name}
                placeholder="Nombre"
                type="text"
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="lastName"
                label="Apellido"
                name="lastName"
                autoComplete="lastName"
                autoFocus
                onChange={handleInputChange}
                value={formComplete.lastName}
                placeholder="lastName"
                type="text"
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="mobile"
                label="Celular"
                name="mobile"
                autoComplete="mobile"
                autoFocus
                onChange={handleInputChange}
                value={formComplete.mobile}
                placeholder="Celular"
                type="number"
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={handleInputChange}
                value={formComplete.email}
                placeholder="Email"
                type="email"
              />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Tipo de documento
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="documentType"
                  name="documentType"
                  value={documentTypee}
                  label="Tipo de documento"
                  onChange={handleChange}
                >
                  <MenuItem value={"CC"}>Cedula de ciudadanía</MenuItem>
                  <MenuItem value={"TI"}>Tarjeta de Identidad</MenuItem>
                  <MenuItem value={"CE"}>Cedula Extranjera</MenuItem>
                </Select>
              </FormControl>
              <TextField
                margin="normal"
                required
                fullWidth
                id="numberDocument"
                label="Numero de identificación"
                name="numberDocument"
                autoComplete="numberDocument"
                autoFocus
                onChange={handleInputChange}
                value={formComplete.numberDocument}
                placeholder="Numero de identificación"
                type="number"
              />

              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={handleInputChange}
                value={formComplete.password}
                autoComplete="current-password"
              />

              <Button
                variant="contained"
                endIcon={<SendIcon />}
                sx={{ mt: 3, mb: 2 }}
                type="submit"
                color="secondary"
                disabled={!formChanged}
              >
                Registrarme
              </Button>
              <Grid container>
                <Grid item>
                  <NavLink to="/singin">
                    Do you have an account? Sign in
                  </NavLink>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
