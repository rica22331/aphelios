
import { Route, Redirect } from "react-router-dom";

// const user=null;

export default function PrivateRoutes({component:Component,...rest}) { 

    const userLocal=window.localStorage.getItem('userToken')
    // console.log(userLocal);
    return (
     <Route {...rest} >
         {userLocal?
        (<Component/>)
            :     ( <Redirect  to="/singin" />)
    }
     </Route>
    )
}
