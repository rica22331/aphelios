/*!

=========================================================
* Paper Kit React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/main/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

// styles
import "bootstrap/scss/bootstrap.scss";
import "assets/scss/paper-kit.scss?v=1.3.0";
import "assets/demo/demo.css?v=1.3.0";
// pages
import Index from "views/Index.js";
import NucleoIcons from "views/NucleoIcons.js";
import LandingPage from "views/examples/LandingPage.js";
import ProfilePage from "views/examples/ProfilePage.js";
import RegisterPage from "views/Auth/RegisterPage.js";
import LoginPage from "views/Auth/loginPage.js";
import PrivateRoutes from "components/privateRoutes";

import PrivateRoutesAuth from "components/privateRoutesAuth";
// others

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <PrivateRoutes path="/dashboard" component={Index} />
      <PrivateRoutesAuth
        path="/singup"
        component={RegisterPage}
      
      />
         <PrivateRoutesAuth
        path="/singin"
        component={LoginPage}

       
      />
      {/* <Route
        path="/nucleo-icons"
        render={(props) => <NucleoIcons {...props} />}
      /> */}
      <PrivateRoutes
        path="/landing-page"
       component={LandingPage}
      />
      <PrivateRoutes
        path="/profile-page"
        component={ProfilePage}

      />
   
      <Redirect to="/dashboard" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
